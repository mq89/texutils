#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import re

__PATTERN = re.compile(r"\\input{(.*)}")


def texfiles(texroot):
    queue = [texroot]
    files = [texroot]
    while len(queue) > 0:
        items = __parse_file(queue.pop())
        queue.extend(items)
        files.extend(items)
    return files


def __parse_file(file):
    result = []
    with open(file) as f:
        for line in f.readlines():
            match = __PATTERN.search(line)
            if match:
                file = match.group(1)
                if file[:-4] != ".tex":
                    file += ".tex"
                result.append(file)
    return result
